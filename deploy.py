from ftplib import FTP
import os


hostname = ""
username = ""
password = ""
project_folder_to_deploy = ""    # must contains "dist" folder


# Change directories - create if it doesn't exist
def chdir(dir): 
    if directory_exists(dir) is False:
        ftp.mkd(dir)
    ftp.cwd(dir)

# Check if directory exists (in current location)
def directory_exists(dir):
    filelist = []
    ftp.retrlines('LIST',filelist.append)
    for f in filelist:
        if f.split()[-1] == dir and f.upper().startswith('D'):
            return True
    return False

# Send file
def send_file(ftp, filename):
    dirs_down = ''
    with open(filename, 'rb') as fp:
        file_splited = filename.split('/')
        if len(file_splited) > 0:
            for item in file_splited:
                if len(item.split('.')) > 1:
                    ftp.storbinary('STOR %s' % os.path.basename(filename), fp, 1024)
                    print(F'uploaded file {filename}')
                else:
                    if item != 'dist':
                        chdir(item)
                        dirs_down += '../'
    ftp.cwd(dirs_down)
            

# Get each file of the folder
def get_each_file(project):
    project_files = []
    for root, dirs, files in os.walk(F"{project}/dist"):
        for file in files:
            project_files.append(os.path.join(root,file))

    return project_files

# Run
with FTP(hostname) as ftp:
    ftp.login(user=username, passwd=password)
    # ftp.set_debuglevel(2)

    files = get_each_file(project_folder_to_deploy)
    for file in files:
        send_file(ftp, file)

